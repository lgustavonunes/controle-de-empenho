<?php include("cabecalho.php");
      include("conecta.php");
      include("banco-empenho.php"); ?>

<?php

$proponente = $_GET["proponente"];
$cpf = $_GET["cpf"];


if(insereProduto($conexao, $proponente, $cpf)) { ?>
    <p class="text-success">O Empenho foi adicionado com sucesso!</p>
<?php } else {
    $msg = mysqli_error($conexao);
?>
    <p class="text-danger">O Empenho não foi adicionado: <?= $msg ?></p>
<?php
}
?>

<?php include("rodape.php"); ?>
