<?php include("cabecalho.php"); ?>

<h1>Formulário de cadastro</h1>
<form action="adiciona-empenho.php">
    <table class="table">
        <tr>
            <td>Proponente</td>
            <td><input class="form-control" type="text" name="proponente" autofocus="" /></td>
        </tr>
        <tr>
            <td>Cpf</td>
            <td><input class="form-control" type="text" name="cpf" /></td>
        </tr>
        
        <tr>
            <td>Valor</td>
            <td><input class="form-control" type="number" name="valor"></td>
        </tr>

        <tr>
            <td>Processo</td>
            <td><input class="form-control" type="text" name="processo"></td>
        </tr>

        <tr>
            <td>Siafi</td>
            <td><input class="form-control" type="text" name="siafi"></td>
        </tr>

        <tr>
            <td><button class="btn btn-primary" type="submit">Cadastrar</button></td>
        </tr>
    </table>
</form>

<?php include("rodape.php"); ?>
