<?php

function listaProdutos($conexao) {
    $empenhos = array();
    $resultado = mysqli_query($conexao, "select * from empenho");

    while($empenho = mysqli_fetch_assoc($resultado)) {
        array_push($empenhos, $empenho);
    }

    return $empenhos;
}

function insereProduto($conexao, $proponente, $cpf) {
    $query = "insert into empenho (proponente, cpf) values ('{$proponente}', {$cpf})";
    return mysqli_query($conexao, $query);
}
